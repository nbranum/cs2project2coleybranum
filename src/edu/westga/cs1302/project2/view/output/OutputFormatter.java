package edu.westga.cs1302.project2.view.output;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import edu.westga.cs1302.project2.model.Sky;
import edu.westga.cs1302.project2.model.WingedInsect;
import edu.westga.cs1302.project2.view.resources.UI;

/**
 * OutputFormatter builds summaries and reports for Skys.
 *
 * @author CS1302
 */
public class OutputFormatter {

	private ArrayList<WingedInsect> butterflies;
	private ArrayList<WingedInsect> flies;

	/**
	 * Initializes the OutputFormatter
	 */
	public OutputFormatter() {
	}

	/**
	 * Creates a report by analyzing a given sky.
	 * 
	 * @precondition sky != null
	 * @postcondition none
	 *
	 * @param sky sky to analyze
	 * @return String with the given report
	 */
	public String buildReport(Sky sky) {
		if (sky == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_SKY);
		}
		String summary = "Sky Report:";
		summary += System.lineSeparator();
		summary += "\n";
		summary += this.numberPerType(sky);
		Collections.sort(sky.getListOfWingedInsects(), new WidthThenSpeedComparator());
		for (int i = 0; i < sky.getListOfWingedInsects().size(); i++) {
			if (i == 0) {
				summary += "Smallest Insect: " + sky.getListOfWingedInsects().get(i).toString();
				summary += System.lineSeparator();
			} else if (i == sky.getListOfWingedInsects().size() - 1) {
				summary += "Largest Insect: " + sky.getListOfWingedInsects().get(i).toString();
				summary += System.lineSeparator();
			}
		}
		summary += "\n";
		summary += "Insects in Sorted Order \n";
		for (WingedInsect insect : sky.getListOfWingedInsects()) {
			summary += insect.toString() + "\n";
		}
		summary += System.lineSeparator();
		summary += this.largestAndSmalledtEach(sky);
		return summary;
	}

	/**
	 * breaks apart the list of winged insects by type
	 * 
	 * @precondition none
	 * @postcondition insects.size() == flies.size() + butterflies.size()
	 * 
	 * @param sky sky to analyze
	 */
	public void splitByType(Sky sky) {
		this.butterflies = new ArrayList<WingedInsect>();
		this.flies = new ArrayList<WingedInsect>();
		for (WingedInsect insect : sky.getListOfWingedInsects()) {
			if (insect.toString().contains("Butterfly")) {
				this.butterflies.add(insect);
				this.butterflies.sort(null);
			}
			if (insect.toString().contains("Fly")) {
				this.flies.add(insect);
				this.flies.sort(null);
			}
		}
	}

	/**
	 * 
	 * returns the string including the number of each type of insect are in the sky
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param sky the specified sky to be analyzed
	 * @return String including the number of each type of insect are in the sky
	 */
	private String numberPerType(Sky sky) {
		int numberButterfly = 0;
		int numberFly = 0;
		String summary = "";
		for (WingedInsect insect : sky.getListOfWingedInsects()) {
			if (insect.toString().contains("Butterfly")) {
				numberButterfly++;
			}
			if (insect.toString().contains("Fly")) {
				numberFly++;
			}
		}
		summary += "Number of Butterflies: " + numberButterfly + "\n";
		summary += "Number of Flies: " + numberFly;
		summary += System.lineSeparator();
		summary += " ";
		summary += System.lineSeparator();
		return summary;
	}

	/**
	 * 
	 * returns the string including the largest and smallest of both types of winged
	 * insect
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param sky the specified sky to be analyzed
	 * 
	 * @return String including the largest and smallest of both types of winged
	 *         insect
	 */
	private String largestAndSmalledtEach(Sky sky) {
		String summary = "";
		this.splitByType(sky);
		summary += "Smallest Butterfly: " + this.butterflies.get(0).toString();
		summary += System.lineSeparator();
		summary += "Largest Butterfly: " + this.butterflies.get(this.butterflies.size() - 1).toString();
		summary += System.lineSeparator();
		summary += "Smalles Fly: " + this.flies.get(0).toString();
		summary += System.lineSeparator();
		summary += "Largest Fly: " + this.flies.get(this.flies.size() - 1).toString();
		summary += System.lineSeparator();
		return summary;
	}

	/**
	 * creates a two level sort using comparator
	 * 
	 * @author nicolebranum
	 *
	 */
	public class WidthThenSpeedComparator implements Comparator<WingedInsect> {

		@Override
		public int compare(WingedInsect o1, WingedInsect o2) {
			if (o1.compareTo(o2) == 0) {
				if (o1.getSpeed() > o2.getSpeed()) {
					return 1;
				} else if (o1.getSpeed() < o2.getSpeed()) {
					return -1;
				} else {
					return 0;
				}
			} else {
				return o1.compareTo(o2);
			}
		}
	}
}

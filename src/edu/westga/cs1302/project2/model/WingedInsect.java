package edu.westga.cs1302.project2.model;

import java.util.Random;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public abstract class WingedInsect extends Canvas implements Flying, Comparable<WingedInsect> {

	private Direction direction;
	private int speed;
	private int flyCount;

	/**
	 * created a new winged insect
	 * 
	 * @precondition speed > 0;
	 * @postcondition super.getWidth()=width super.getHeight()=height
	 *                super.getLayoutX()=x super.getLayoutY()=y
	 * 
	 * @param x      top left x coordinate
	 * @param y      top left y coordinate
	 * @param width  width of canvas where insect is placed
	 * @param height height of canvas where insect is placed
	 * @param speed  the speed of the winged insect
	 */
	protected WingedInsect(double x, double y, double width, double height, int speed) {
		super(width, height);
		super.setLayoutX(x);
		super.setLayoutY(y);
		if (speed <= 0) {
			throw new IllegalArgumentException();
		}
		this.speed = speed;
		Random random = new Random();
		int value = random.nextInt(2);
		if (value == 1) {
			this.direction = Direction.RIGHT;
		} else {
			this.direction = Direction.LEFT;
		}
	}

	@Override
	public void fly() {
		double currentX = this.getLayoutX();
		if (this.getDirection().equals(Direction.RIGHT)) {
			currentX += this.getSpeed();
			if (currentX >= 750) {
				currentX = 0;
			}
			this.setLayoutX(currentX);
		}
		if (this.getDirection().equals(Direction.LEFT)) {
			currentX -= this.getSpeed();
			if (currentX <= 0) {
				currentX = 750;
			}
			this.setLayoutX(currentX);
		}
		this.flyCount++;
	}

	/**
	 * gets the number of times fly() has been called
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return int representing the number of times fly() has been called
	 */
	public int getFlyCount() {
		return this.flyCount;
	}

	/**
	 * gets the speed that the insect is heading
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return int the speed the insect is going
	 */
	public int getSpeed() {
		return this.speed;
	}

	/**
	 * gets the direction that the insect is heading
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return direction the direction the insect is going
	 */
	public Direction getDirection() {
		return this.direction;
	}

	/**
	 * clears the canvas
	 * 
	 * @precondition none
	 * @postcondition super.getWidth()=default value super.getWidth()=default value
	 *                super.getLayoutX()=0 super.getLayoutY()=0
	 * 
	 */
	public void clear() {
		GraphicsContext gc = this.getGraphicsContext2D();
		gc.clearRect(0, 0, this.getWidth(), this.getHeight());
	}

	/**
	 * sorts insects by ascending width
	 * 
	 * @precondition none
	 * @postcondition insects.get(i).getWidth() <= insects.get(i+1).getWidth();
	 * 
	 * @param insect2 the insect being compared
	 */
	@Override
	public int compareTo(WingedInsect insect2) {
		if (this.getWidth() > insect2.getWidth()) {
			return 1;
		} else if (this.getWidth() < insect2.getWidth()) {
			return -1;
		} else {
			return 0;
		}
	}
	
	/**
	 * will draw the specified winged insect
	 * 
	 * @precondition none
	 * @postconditon none
	 * 
	 */
	abstract void draw();
}

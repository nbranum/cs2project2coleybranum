package edu.westga.cs1302.project2.model;
/**
 * this enum lists valid directions
 * 
 * @author nicolebranum
 *
 */
public enum Direction {
	LEFT, RIGHT
}

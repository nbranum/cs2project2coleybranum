package edu.westga.cs1302.project2.model;
/**
 * interface flying 
 * 
 * @author nicolebranum
 *
 */
public interface Flying {
	/**
	 * method will be used to move objects / have them "fly" across the screen
	 */
	void fly();
}

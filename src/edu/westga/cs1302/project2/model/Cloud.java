package edu.westga.cs1302.project2.model;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * cloud class
 * 
 * @author nicolebranum
 *
 */
public class Cloud extends Canvas implements Flying {

	private int speed;
	
	/**
	 * 
	 * creates a cloud object
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param y      top left y coordinate
	 * @param width  width of the cloud
	 * @param height height of the cloud
	 * @param speed  int speed the butterfly is traveling
	 */
	public Cloud(double y, double width, double height, int speed) {
		super(width, height);
		super.setLayoutX(750 - this.getWidth());
		super.setLayoutY(y);
		if (speed <= 0) {
			throw new IllegalArgumentException();
		}
		this.speed = speed;
		this.draw();
	}

	/**
	 * gets the speed that the cloud is moving
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return int the speed the cloud is going
	 */
	public int getSpeed() {
		return this.speed;
	}
	
	@Override
	public void fly() {
		double currentX = this.getLayoutX();
		currentX -= this.getSpeed();
		if (currentX <= 0) {
			currentX = 750;
		}
		this.setLayoutX(currentX);
	}

	/**
	 * draws a cloud object
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	public void draw() {
		GraphicsContext gc = super.getGraphicsContext2D();
		this.drawBase(gc);
		this.drawPuffs(gc);
	}

	/**
	 * draws the cloud base
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawBase(GraphicsContext gc) {
		gc.setFill(Color.WHITE);
		gc.fillRoundRect(0, this.getHeight() / 2, this.getWidth(), this.getHeight() / 2, this.getWidth() / 2,
				this.getHeight() / 2);
	}

	/**
	 * draws the cloud puffs
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawPuffs(GraphicsContext gc) {
		gc.setFill(Color.WHITE);
		double fifthOfWidth = this.getWidth() / 5;
		double fourthOfHeight = this.getHeight() / 4;
		gc.fillOval(fifthOfWidth, this.getHeight() / 2 - fourthOfHeight, fifthOfWidth, this.getHeight() / 2);
		gc.fillOval(fifthOfWidth * 2, this.getHeight() / 2 - this.getHeight() / 2, fifthOfWidth, this.getHeight());
		gc.fillOval(fifthOfWidth * 3, this.getHeight() / 2 - fourthOfHeight, fifthOfWidth, this.getHeight() / 2);

	}

}

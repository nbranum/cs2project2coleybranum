package edu.westga.cs1302.project2.model.manager;

import java.util.Random;

import edu.westga.cs1302.project2.model.Butterfly;
import edu.westga.cs1302.project2.model.Cloud;
import edu.westga.cs1302.project2.model.Fly;
import edu.westga.cs1302.project2.model.Flying;
import edu.westga.cs1302.project2.model.Sky;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * The Class SkyManager.
 * 
 * @author CS1302
 */
public class SkyManager extends Pane {

	private Sky sky;
	private Color[] colors = { Color.RED, Color.PURPLE, Color.GREEN, Color.ORANGE };

	/**
	 * Instantiates a new sky.
	 * 
	 * @precondition none
	 * @postcondition getSky() != null
	 */
	public SkyManager() {
		this.sky = new Sky();
		this.getChildren().add(this.sky);
	}

	/**
	 * Gets the sky.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sky
	 */
	public Sky getSky() {
		return this.sky;
	}

	/**
	 * Sets the up bugs and clouds.
	 * 
	 * @precondition nInsects >= 0
	 * @postcondition getSky().getInsects().getSize() == nInsects
	 */
	public void setupBugsAndClouds() {
		this.generateInsects();
		this.generateClouds();
	}

	/**
	 * Generate insects.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */

	private void generateInsects() {
		for (int i = 0; i < 8; i++) {
			int size = this.generateSize();
			int x = this.generateRandomX(size);
			int y = this.generateInsectRandomY(size);
			int speed = this.generateRandomSpeed();
			String type = this.generateRandomInsectType();
			if (type.equalsIgnoreCase("Butterfly")) {
				Color color = this.generateRandomColor();
				Butterfly butterfly = new Butterfly(x, y, size, color, speed);
				this.sky.addInsect(butterfly);
				this.getChildren().add(butterfly);
			}
			if (type.equalsIgnoreCase("Fly")) {
				Fly fly = new Fly(x, y, size, speed);
				this.sky.addInsect(fly);
				this.getChildren().add(fly);
			}

		}
	}

	/**
	 * Generates random int for the speed of the WingedInsect
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return int speed
	 * 
	 */
	private int generateRandomSpeed() {
		Random random = new Random();
		return random.nextInt(21) + 10;

	}

	/**
	 * Generates random type for the WingedInsect
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return String representation of insect type
	 * 
	 */
	private String generateRandomInsectType() {
		Random random = new Random();
		double number = random.nextDouble();
		if (number <= 0.75) {
			return "Butterfly";
		} else {
			return "Fly";
		}
	}

	/**
	 * Generates random color for butterfly
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Color the color for the butterfly
	 * 
	 */
	private Color generateRandomColor() {
		Random random = new Random();
		int index = random.nextInt(this.colors.length);
		return this.colors[index];
	}

	/**
	 * Generates random x for butterfly
	 * 
	 * @precondition none
	 * @postcondition x+size < width
	 * 
	 * @param double size the size of the butterfly
	 * @return double x the upper left x coordinate
	 * 
	 */
	private int generateRandomX(int size) {
		Random random = new Random();
		int x = random.nextInt(Sky.WIDTH - size);
		return x;
	}

	/**
	 * Generates random y for insect
	 * 
	 * @precondition none
	 * @postcondition y+size < height
	 * 
	 * @param double size the size of the insect
	 * @return double y the upper left y coordinate
	 * 
	 */
	private int generateInsectRandomY(int size) {
		int insectMin = 450 / 3;
		Random random = new Random();
		int y = (random.nextInt((450 - insectMin) + 1) + insectMin) - size;
		return y;
	}

	/**
	 * Generates random size for butterfly
	 * 
	 * @precondition none
	 * @postcondition 50<size<100
	 * 
	 * @return int size the size of the butterfly
	 * 
	 */
	private int generateSize() {
		Random random = new Random();
		int size = random.nextInt(50) + 50;
		return size;
	}

	/**
	 * Generates random y for butterfly
	 * 
	 * @precondition none
	 * @postcondition y+size < height
	 * 
	 * @param double size the size of the butterfly
	 * @return double y the upper left y coordinate
	 * 
	 */
	private int generateCloudRandomY(int height) {
		int cloudMax = 450 / 3;
		Random random = new Random();
		int y = (random.nextInt(cloudMax + 1));
		return y;
	}

	/**
	 * Generate clouds.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	private void generateClouds() {
		int width = this.generateSize();
		int height = this.generateSize();
		int y = this.generateCloudRandomY(height);
		int speed = this.generateRandomSpeed();
		Cloud cloud = new Cloud(y, width, height, speed);
		this.sky.addCloud(cloud);
		this.getChildren().add(cloud);

	}

	/**
	 * Move all flying objects in the sky
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void update() {
		for (Flying flyingObject : this.sky.updateFlyingArray()) {
			flyingObject.fly();
		}
	}
}

package edu.westga.cs1302.project2.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * The Class Sky.
 * 
 * @author CS1302
 */
public class Sky extends Rectangle implements Iterable<Flying> {

	public static final int WIDTH = 750;
	public static final int HEIGHT = 450;

	private List<WingedInsect> insects;
	private List<Cloud> clouds;
	private List<Flying> flying;

	/**
	 * Instantiates a new sky.
	 * 
	 * @precondition none
	 * @postcondition insects.sze() == 0 && clouds.size() == 0
	 */
	public Sky() {
		super(0, 0, Sky.WIDTH, Sky.HEIGHT);
		this.setFill(Color.CORNFLOWERBLUE);
		this.insects = new ArrayList<WingedInsect>();
		this.clouds = new ArrayList<Cloud>();
		this.flying = new ArrayList<Flying>();
		this.updateFlyingArray();
	}

	/**
	 * gives the list of winged insects
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return List the collection of winged insects
	 */
	public List<WingedInsect> getListOfWingedInsects() {
		return this.insects;
	}

	/**
	 * gives the list of clouds
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return List the collection of clouds
	 */
	public List<Cloud> getListOfClouds() {
		return this.clouds;
	}

	/**
	 * adds specified winged insect to the collection
	 * 
	 * @precondition none
	 * @postcondition insects.size++
	 * 
	 * @param insect the insect to be added
	 */
	public void addInsect(WingedInsect insect) {
		this.insects.add(insect);
	}

	/**
	 * adds specified cloud to the collection of clouds
	 * 
	 * @precondition none
	 * @postcondition clouds.size++
	 * 
	 * @param cloud the cloud to be added
	 */
	public void addCloud(Cloud cloud) {
		this.clouds.add(cloud);
	}

	/**
	 * updates the array list of flying object
	 * 
	 * @precondition none
	 * @postcondition flying.size() == insects.size() + clouds.size()
	 * 
	 * @return List<Flying> including all flying objects
	 * 
	 */
	public List<Flying> updateFlyingArray() {
		this.flying.addAll(this.clouds);
		this.flying.addAll(this.insects);
		return this.flying;
	}

	@Override
	public Iterator<Flying> iterator() {
		return this.flying.iterator();
	}

}

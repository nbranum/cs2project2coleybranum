package edu.westga.cs1302.project2.model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * fly class
 * 
 * @author nicolebranum
 *
 */
public class Fly extends WingedInsect {

	/**
	 * 
	 * creates a fly object
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param x     top left x coordinate
	 * @param y     top left y coordinate
	 * @param size  width and height of butterfly
	 * @param speed int speed the butterfly is traveling
	 */
	public Fly(double x, double y, double size, int speed) {
		super(x, y, size, size, speed);
		this.draw();
	}

	@Override
	void draw() {
		GraphicsContext gc = super.getGraphicsContext2D();
		this.drawBody(gc);
		this.drawWings(gc);
		this.drawEyes(gc);
		this.drawNose(gc);
	}

	/**
	 * 
	 * draws the body of the fly
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawBody(GraphicsContext gc) {
		gc.setFill(Color.BLACK);
		gc.fillOval(this.getWidth() / 3, this.getHeight() / 3, this.getWidth() / 3, this.getHeight() / 3);
	}

	/**
	 * 
	 * draws the wings of the fly
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawWings(GraphicsContext gc) {
		gc.setStroke(Color.BLACK);
		gc.strokeOval(0, this.getHeight() / 3, this.getWidth(), this.getHeight() / 3);
	}

	/**
	 * 
	 * draws the eyes of the fly
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawEyes(GraphicsContext gc) {
		gc.setStroke(Color.WHITE);
		gc.strokeLine(this.getWidth() / 2 - 5, this.getHeight() / 2 - 7.5, this.getWidth() / 2 - 5,
				this.getHeight() / 2 - 7.5);
		gc.strokeLine(this.getWidth() / 2 + 5, this.getHeight() / 2 - 7.5, this.getWidth() / 2 + 5,
				this.getHeight() / 2 - 7.5);
	}

	/**
	 * 
	 * draws the nose of the fly which shows its direction of travel
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawNose(GraphicsContext gc) {
		gc.setFill(Color.WHITE);
		if (super.getDirection().equals(Direction.LEFT)) {
			double[] xValues = { this.getWidth() / 2 - 5, this.getWidth() / 2, this.getWidth() / 2 };
			double[] yValues = { this.getHeight() / 2, this.getHeight() / 2 - 3, this.getHeight() / 2 + 3 };
			gc.fillPolygon(xValues, yValues, 3);
		}
		if (super.getDirection().equals(Direction.RIGHT)) {
			double[] xValues = { this.getWidth() / 2 + 5, this.getWidth() / 2, this.getWidth() / 2 };
			double[] yValues = { this.getHeight() / 2, this.getHeight() / 2 - 3, this.getHeight() / 2 + 3 };
			gc.fillPolygon(xValues, yValues, 3);
		}
	}

	@Override
	public String toString() {
		return "Fly Width: " + this.getWidth() + " Speed: " + this.getSpeed();
	}

}

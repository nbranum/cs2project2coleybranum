package edu.westga.cs1302.project2.model;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Butterfly extends WingedInsect {
	private Color color;

	/**
	 * creates a butterfly
	 * 
	 * @precondition none
	 * @postcondition top left coordinates of insect are (x,y) width and height of
	 *                insect = size color of insect = specified color
	 * 
	 * @param x     top left x coordinate
	 * @param y     top left y coordinate
	 * @param size  width and height of butterfly
	 * @param color color of the butterfly
	 * @param speed int speed the butterfly is traveling
	 */
	public Butterfly(double x, double y, double size, Color color, int speed) {
		super(x, y, size, size, speed);
		this.color = color;
		this.draw();
	}

	/**
	 * gets the color of the butterfly
	 * 
	 * @return Color the color of the butterfly
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * gives a string representation of the butterfly
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a string giving the width and color of the butterfly
	 */
	public String toString() {
		return "Butterfly Width:" + this.getWidth() + " Speed: " + this.getSpeed() + " Color:" + this.getColor();
	}

	@Override
	public void draw() {
		GraphicsContext gc = super.getGraphicsContext2D();
		this.drawBody(gc);
		this.drawAntennae(gc);

	}

	/**
	 * 
	 * draws the body of the butterfly
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawBody(GraphicsContext gc) {
		gc.setFill(Color.GREY);
		double y = this.getHeight() / 2 - 5;
		gc.fillOval(0, y, this.getWidth(), 10);
	}

	/**
	 * 
	 * draws the antennae of the butterfly which shows its direction of travel
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawAntennae(GraphicsContext gc) {
		Direction direction = super.getDirection();
		gc.setStroke(Color.GREY);
		if (direction.equals(Direction.LEFT)) {
			double y = this.getHeight() / 2 - 5;
			gc.strokeLine(0, y, 8, y + 8);
		} else {
			double y = this.getHeight() / 2 - 5;
			gc.strokeLine(this.getWidth(), y, this.getWidth() - 8, y + 8);
		}
	}

	/**
	 * 
	 * draws the wing of the butterfly upwards
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawWingUp(GraphicsContext gc) {
		gc.setFill(this.getColor());
		double x1 = 0;
		double x2 = this.getWidth();
		double x3 = this.getWidth() / 2;
		double[] xCoords = { x1, x2, x3 };
		double y1 = 0;
		double y2 = 0;
		double y3 = this.getHeight() / 2;
		double[] yCoords = { y1, y2, y3 };
		gc.fillPolygon(xCoords, yCoords, 3);
	}

	/**
	 * 
	 * draws the wing of the butterfly downwards
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param gc
	 */
	private void drawWingDown(GraphicsContext gc) {
		gc.setFill(this.getColor());
		double x1 = 0;
		double x2 = this.getWidth();
		double x3 = this.getWidth() / 2;
		double[] xCoords = { x1, x2, x3 };
		double y1 = this.getHeight();
		double y2 = this.getHeight();
		double y3 = this.getHeight() / 2;
		double[] yCoords = { y1, y2, y3 };
		gc.fillPolygon(xCoords, yCoords, 3);
	}

	@Override
	public void fly() {
		super.fly();
		GraphicsContext gc = super.getGraphicsContext2D();
		if (super.getFlyCount() % 2 == 0) {
			this.clear();
			this.draw();
			this.drawWingUp(gc);
		} else if (super.getFlyCount() % 2 == 1) {
			this.clear();
			this.draw();
			this.drawWingDown(gc);
		}
	}

}
